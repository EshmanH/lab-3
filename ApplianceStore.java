import java.util.Scanner;
public class ApplianceStore{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in); 
        //create 4 microwaves
        Microwave microwaveArr[] = new Microwave[4];
        for (int i = 0; i < microwaveArr.length; i++) {

            System.out.println("Microwave" + (i+1));
            microwaveArr[i] = new Microwave(scan.nextInt(), scan.nextInt(), scan.nextInt(), scan.next());
        }
        //print values inside of microwave object
        for (int i = 0; i < microwaveArr.length; i++) {
            System.out.println("Microwave" + (i+1) + ": " + microwaveArr[i].toString());
        }
        //method 1 
        for (int i = 0; i < microwaveArr.length; i++) {
        System.out.println("Microwave" + (i+1) + ": " + microwaveArr[i].printBrandAndWattage());
        }

        //plate measurements
        System.out.println("Enter plate width");
        int plateWidth = scan.nextInt();
        System.out.println("Enter plate height");
        int plateHeight = scan.nextInt();
        //willfit method
        for (int i = 0; i < microwaveArr.length; i++) {
            System.out.println("Microwave" + (i+1) + ": " + microwaveArr[i].willFit(plateWidth, plateHeight));
        }

        //set timer method
        System.out.println("Enter timer for microwave.");
        int timeInMinutes = scan.nextInt();
        microwaveArr[1].setTimer(timeInMinutes);
    }
}